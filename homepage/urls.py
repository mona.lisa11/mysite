from django.urls import path
from . import views

app_name = 'hompage'

urlpatterns = [
    path('', views.index, name='index'),
]
