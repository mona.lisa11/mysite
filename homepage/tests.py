from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .views import *

# Create your tests here.
class UnitTest(TestCase):
	
	def test_home_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_home_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_calling_right_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)
